#include <iostream>
#include <string>
#include <vector>
using namespace std;

//这个题目没看懂
int lcs(string s1, string s2){

    auto result = 0;
    int len1 = s1.length() + 1;
    int len2 = s2.length() + 1;
    
    //1.定义数组元素的含义
    //dp[i][j] 表示字符串a[1...i]和字符串b[1...j]的最大公共字串长度。
    vector<vector<int> > dp = vector<vector<int> >(len2, vector<int>(len1, 0));
    //3.定义初始值 接上循环

    int i = 1, j = 1;

    //2.定义数组之间的关系式  找出边界值和循环
    //如果a[i] == a[j]， 那么dp[i][j] = dp[i-1][j-1] + 1; 否则 dp[i][j] = 0；

    for (i = 1; i < len2; ++i) {
        for(j = 1; j < len1; j++) {
            if (s2[i - 1] == s1[j - 1])
            {
                dp[i][j] = dp[i -1][j - 1] + 1;
                result = max (result, dp[i][j]);
            }
        }
    }
    return result;
}


int main(){
    string a,b;
    while (cin>> a >>b)
    {
        cout << lcs(a,b) << endl;
    }
    return 0;
}