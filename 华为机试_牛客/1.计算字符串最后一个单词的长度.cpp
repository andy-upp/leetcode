#include <iostream>
#include <string>

int main()
{
    std::string str;
    while(getline(std::cin,str))
    {
        int count = 0;
        for(int i = str.size()-1 ; i >= 0; i--)
        {
            if (str[i] != ' ')
            {
                count ++;
            }else{
                break;
            }
        }
        std::cout << count << std::endl;
    }
    return 0;
}