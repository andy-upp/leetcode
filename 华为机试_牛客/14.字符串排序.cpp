#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;


void myPrintf(string str){
    cout << str << endl;
}

int main(){

    int n;
    while (cin>>n)
    {
        vector<string> vec;
        for(int i = 0; i < n; i++){
            string str;
            cin>> str;
            vec.push_back(str);
        }
        sort(vec.begin(),vec.end());
        // for(string s: vec)
        //     cout << s << endl;
        for_each(vec.begin(), vec.end(), myPrintf); 

    }
    return 0;
    
}


