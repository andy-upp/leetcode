#include <iostream>
using namespace std;

int main(){
    int n;
    while (cin>>n)
    {
        int count = 0;
        while(n>0){
            count += (n%2);
            n /= 2;
        }
        cout << count << endl;
    }
    return 0;
}

