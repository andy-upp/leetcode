#include <iostream>
#include <vector>

using namespace std;

//没看懂题意

int main(){
    
    int m , n;
    while(cin >> m && cin >> n){

        cout << "m:" << m << "n:" << n << endl;
        vector<vector<int> > dp(m, vector<int>(n));

        //第三步 找出初始值 接上循环
        for(int i = 0; i < m; ++i){
            dp[i][0] = 1;
        }

        for(int j = 0; j < n; ++j){
            dp[0][j] = 1;
        }
        
        //第二步 关系数组之间的关系式,并写出循环,i j 都从1开始，到m-1，n-1结束，因为为0时只有一条确定路径，且不符合
        //状态转移方程
        for(int i = 1; i < m; ++i){
            for(int j = 1; j < n; ++j){
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        cout <<  dp[m-1][n-1] << endl;
        
    }
    
    
    return 0;
}