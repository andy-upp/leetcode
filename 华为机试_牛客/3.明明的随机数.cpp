#include <iostream>
#include <set>
#include <string>
using namespace std;

int main()
{
    int n;
    while(cin>>n)
    {
        set<int> a;
        int b;
        for(int i = 0; i < n; i++)
        {
            cin>>b;
            a.insert(b);
        }
        
        for(auto it : a)
        {
            cout << it << endl;
        }
    }
}