//本题可以通过遍历每一个约数，求和，判断完全数。约数计算可以遍历sqrt(n)的范围。
//没写
#include <iostream>
#include <algorithm>
using namespace std;
int Count(int n){
    int count = 0;
    if(n < 0 || n > 500000){
        return -1;
    }
    for(int i = 2; i <= n; ++i){
        int sum = 0;
        for(int j = 2; j <= sqrt(i); ++j){
            if(i % j == 0){
                //如果j^2 = i，说明两个约数相同，只加一个
                if(i / j == j){
                    sum += j;
                }
                else{
                    //否则，两个不同的约数都要相加
                    sum += j + (i / j);
                }
            }
        }
        if(sum + 1 == i){
        ++count;
        }
    }
    return count;
}
int main(){
    int n;
    while(cin >> n){
        cout << Count(n) << endl;
    }
    return 0;
}