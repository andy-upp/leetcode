#include <iostream>
#include <vector>
using namespace std;
int main(){
    
    int n;
    while(cin>>n){
        vector<int>  vec;
        for(int i= 0; i < n; ++i){
            int m;
            cin >> m;
            vec.push_back(m);
        }
        
       //第一步 定义数组元素的含义 dp[i] 表示以i为结尾的最长子序列的长度
       vector<int> dp(n,1);        

       //第三步 确定初始值，接上循环
        int ret = 0;
        
       //第二步 确定不同数组之间的关系式 写出循环
        //此处dp[i]表示前i个状态中最长子序列的长度，则与之前的结果无直接联系 无法递推
        //所以 dp[i] 应该表示以i为结尾的最长子序列的长度, 如果vec[i]大于d[0] - d[i-1]最后一个元素， 将vec[i]放到d[0] - d[i-1]的结尾，
        //然后遍历，找最长的那个序列
         for(int i=0; i < n; ++i){
             for(int j = 0; j < i; ++j){
                 if(vec[i] > vec[j]){
                     dp[i] = max(dp[i],1+dp[j]);  //这里不是太明白
                 }
             }
         }
        for(const auto& i:dp){
            ret = ret > i ? ret:i;
        }
        cout << ret << endl;
 
    }
}