#include<iostream>
#include<stack>
using namespace std;

int main(){
    string str;
    stack<string> s;
    while(getline(cin,str)){ //输入一个句子 while循环一次
    //while(cin >> str){   输入一个句子 按照空格循环n次
        cout << "str:" << str << endl;
        int n = str.size();
        int index = 0;
        for(int i=0;i<n;i++){
            if(str[i]==' '){
                s.push(str.substr(index,i-index));
                index = i+1;
            }
        }

        s.push(str.substr(index)); //push最后一个字符
        while(!s.empty()){
            string res = s.top();
            cout<<res<<" ";
            s.pop();
        }
        cout<<endl;
    }
}

