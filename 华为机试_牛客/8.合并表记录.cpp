#include <iostream>
#include <map>
using namespace std;

int main() {
    int n;
    //while 回车后结束等待 进入循环处理
    while(cin >> n) {
        map<int, int> map;
        for(int i = 0; i < n; i ++) {
            int a, b;
            cin >> a >> b;
            map[a] += b;
        }
        for(auto it : map) cout << it.first << " " << it.second <<endl;
    }
    return 0;
}