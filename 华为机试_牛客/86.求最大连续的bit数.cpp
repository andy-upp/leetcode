#include <iostream>
using namespace std;

int main(){
    int n;
    while(cin >> n){
        int k=0;
        for(k=0;n!=0;k++){//遇到进制的问题，多考虑用位运算
            n=n&(n<<1);//求n与n移位后按位与的结果，如果连续的1的个数为1，则更新后的n为0；
        }//如果有多个连续的1，则按位与之后其他位都为0，而连续1的个数减1；依次循环，直到n为0，k记录了移位多少次，即为连续1的个数
        cout << k <<endl;
    }
    return 0;
}



