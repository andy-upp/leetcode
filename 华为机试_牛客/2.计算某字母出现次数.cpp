#include <iostream>
#include <string>
#include <cctype>
 
int main()
{
    std::string str;
    char s;
 
    while(getline(std::cin,str))
    {
      if(std::cin.get(s).get())
         {
             int count = 0;
             for(int i = 0; i < str.size(); i++ )
             {
                if(toupper(str[i]) == toupper(s))
                {
                    count++;
                }
            }
              std::cout << count <<std::endl;
         }
    }       
    return 0;
}