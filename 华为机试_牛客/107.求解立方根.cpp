/*
1的立方小于2,而bai2的立方du大于2，所以zhi2开立方应该在1和2之间，
因为dao是用zhuan二分法，所以取1.5，计shu算1.5的立方大于2，所以2开立方应该在1和1.5之间，
根据二分法，再取1和1.5的中间数，1.25，计算1.25的立方等于1.953，小于2，所以2开立方应该在1.25和1.5之间，
根据二分法，再取1.25和1.5的中间数，1.375，计算1.375的立方等于2.5996，大于2，所以2开立方应该在1.25和1.375之间
再根据二分法去1.25和1.375的中间数，1.3125，以此类推
*/

#include <iostream>
using namespace std;

double getgen(double a){
    double max = a;
    double mid = 0;
    double min = 0;
    if(a < 1)
        max = 1;
    while ((max - min) > 0.00001){
        mid = (max + min)/2;
        if(mid*mid*mid > a){
            max = mid;
        }else{
            min = mid;
        }
    }
    return max;
}





int main(){
    double a;
    cin >> a;
    if(a >= 0){
        printf("%0.1f",getgen(a));  //保留一位小数
    }else{
        printf("%0.1f",-getgen(-a));
    }
    return 0;
}