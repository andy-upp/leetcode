#include <iostream>
#include <set>
#include <string>
#include <unordered_set>
using namespace std;

int main(){
    int nums;
    cin >> nums;
    string str = to_string(nums);
    unordered_set<char> set;//非排序无重复
    reverse(str.begin(), str.end());
    string res;
    for(char c : str)
       if(set.count(c) != 1)
       {
           res += c;
           set.insert(c);
       }
    cout << stoi(res) << endl;
    return 0;
}

















