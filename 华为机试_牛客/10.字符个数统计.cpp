#include <iostream>
#include <unordered_set>

using namespace std;

int main(){
    string str;
    cin>>str;
    unordered_set<char>  set;
    for(char c : str)
        set.insert(c);
    cout << set.size() << endl;
    return 0;
}
