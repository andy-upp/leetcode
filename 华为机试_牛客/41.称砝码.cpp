//这个题目的例子没看懂

/*

怎么去重，用set集合。

首先根据输入顺序，将砝码用数字序列表示，例如2个1g和1个2g，就用 1 1 2的序列表示；
set序列用来表示加入当前砝码之前能产生的重量种类；
set初始化为{0}；当第一个1g砝码放入时，则set中需要插入原先set中的所有元素+1g后的结果；即{0, 0+1};
当第二个1g加入时，则set会插入{0+1, 1+1},就变成了{0, 1, 2};
重复上述步骤加入所有砝码；则最后set的大小即为能产生的重量种类。

*/
#include <iostream>
#include <vector>
#include <set>
using namespace std;

int main(){
    int n, a[10], tmp;
    while(cin >> n){
        vector<int> v;
        set<int> s;
        for(int i = 0; i < n; i++) cin >> a[i];
        for(int i = 0; i < n; i++){
            cin >> tmp;
            for(int j = 0; j < tmp; j++) v.push_back(a[i]);
        }
        s.insert(0);
        for(int i = 0; i < v.size(); i++){
            set<int> t(s);
            for(auto it = t.begin(); it != t.end(); it++)
            {
                s.insert(*it + v[i]);
            }
        }
        cout << s.size() << endl;
    }
    return 0;
}
