#include <iostream>
using namespace std;

//思路 两数的最大公倍数等于两个数的成集除以两个数的最大公倍数
//辗转相除法求最大数
int gcd(int a, int b){
    
    while (a%b)
    {
        int tmp = a;
        a = b;
        b = tmp%b;
    }
    return b;
}


int main(){

    int a;
    int b;
    while (cin>>a>>b)
    {
        cout << a*b/gcd(a,b) << endl;
    }
    
}