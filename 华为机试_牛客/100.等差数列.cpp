#include <iostream>
using namespace std;

int main(){
    int n;
    while(cin >> n){
        if (n <= 0){
            cout << 0;
        }
        int an = 0;
        //用等差数列前n项和的公式计算
        an = 2 + 3*(n-1);
        cout << n*(2 + an)/2 << endl;
    }
    return 0;
}