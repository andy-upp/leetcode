#include <iostream>
using namespace std;

int main(){
    int n;
    while(cin>>n){
        int count = 0;
        int num1;
        int num2;
        while(n>=3){
            num1 = n/3;    
            num2 = n%3;
            count += num1;
            n = num1 + num2;
            if(n == 2){
                count += 1;
                cout << count << endl;
            }else if(n == 1){
                cout << count << endl;
            }
        }
    }
    return 0;
}