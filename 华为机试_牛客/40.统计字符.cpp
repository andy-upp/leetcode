#include <iostream>
#include <string>
using namespace std;

int main(){
    
    string str;
    while(getline(cin,str)){
        int a = 0, b = 0, c = 0, d = 0;
        int len = str.size();
        for(int i = 0; i < len; ++i){
            if(isalpha(str[i])){
                a++;
            }else if(isspace(str[i])){
                b++;
            }else if (isdigit(str[i])){
                c++;
            }else if (ispunct(str[i])){
                d++;
            }
        }
         cout << a << endl << b << endl << c << endl << d << endl;

    }
    
    return 0;
}