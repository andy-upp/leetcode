/*
isupper()判断一个字符是否是大写字母
isalpha()判断一个字符是否是字母
isblank()判断一个字符是否是空白符
isdigit()判断一个字符是否是十进制数字
islower()判断一个字符是否是小写字母
isspace()判断一个字符是否是空白符
tolower()将大写字母转换为小写字母
toupper()将小写字母转换为大写字母
*/
#include <iostream>
#include <string>
using namespace std;

int main(){
    string str;
    while(getline(cin,str)){
        int num = 0;
        for(int i=0; i < str.size(); i++){
            if(isupper(str[i])){
                num += 1;
            }
        }
        cout << num << endl;
    }
    return 0;
}