//https://www.jianshu.com/p/32eff53a74ca
#include <iostream>
using namespace std;


//链表表示方式
struct ListNode
{
    int val;
    ListNode* next;
    ListNode(int x):val(x),next(nullptr){}
};

//创建一个无头节点的链表
ListNode* initLink()
{
    ListNode* p = nullptr;
    ListNode* temp = (ListNode*)malloc(sizeof(ListNode));
    temp->val = 0;
    temp->next = nullptr;
    p = temp;
    for(int i = 1; i < 4; ++i)
    {
        ListNode* a = (ListNode*)malloc(sizeof(ListNode));
        a->val = i;
        a->next = nullptr;
        temp->next = a;
        temp = temp->next;
    }
    return p;
}
//创建一个有头节点的链表
ListNode* initLinkwithHead()
{
    ListNode* p = (ListNode*)malloc(sizeof(ListNode));
    ListNode* temp = p;
    p = temp;
    for(int i = 0; i < 4; ++i)
    {
        ListNode* a = (ListNode*)malloc(sizeof(ListNode));
        a->val = i;
        a->next = nullptr;
        temp->next = a;
        temp = temp->next;
    }
    return p;
}




int main()
{

    ListNode* p = initLinkwithHead();
    for(int i = 0; i < 4; ++i)
    {
        cout << p->val << endl;
        ++p;
    }

    return 0;
}