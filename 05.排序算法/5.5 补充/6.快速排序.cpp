#include <iostream>
#include <vector>
using namespace std;
//https://www.cnblogs.com/ttss/p/4115708.html

template<typename datatype>
void myQuickSort(vector<datatype>& vec, int low, int hign )
{
    if (low < hign)
    {
        int l = low;
        int r = hign;
        datatype key = vec[l];
        while (l < r)
        {
            while (l < r && key <= vec[r])
                --r;
            vec[l] = vec[r];
            
            while (l < r && key >= vec[l])
                ++l;
            vec[r] = vec[l];            
        }
        vec[l] = key;

        myQuickSort(vec, low, l - 1);
        myQuickSort(vec, r + 1, hign);
    }
}

void printVector(vector<int>& v)
{
    for(vector<int>::iterator it = v.begin(); it != v.end(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;
}


int main()
{
   int len = 30;
  //    int array[] = {9,8,1,7,2,8,14,6};
  // vector<int> vec(begin(array), end(array));
  vector<int> data;
   for(int i = 0; i < len; ++i)
   {    
       data.push_back(rand()%100);
       if ((i+1)%10 == 0)
            cout << endl;
   }
   printVector(data);
   myQuickSort(data, 0, len - 1);
   cout << "==========" << "排序后" << "==========" << endl;
   printVector(data);
}