#include<iostream>
#include<vector>
using namespace std;

class Solution{
public:
    int rob(const vector<int>& nums)
    {
        if(nums.size() == 0)
            return 0;

        int n = nums.size();
        vector<int> dp(n+1,0);
        dp[0] = 0;
        dp[1] = nums[0];
        for(int k = 2; k <= nums.size(); ++k)
        {
            //注意下标和递归公司参数的区别
            dp[k] = max(dp[k-1], nums[k-1]+dp[k-2]);
        }
        return dp[n];
    }
};

int main()
{
    vector<int> nums = {2,7,9,3,1};
    Solution s;
    int vec = s.rob(nums);
    cout << "vec:" << vec << endl;
    return 0;
}