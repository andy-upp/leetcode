#include<iostream>
#include<vector>
using namespace std;


class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& A) {
        if(A.size() < 3)
            return 0;
        
        int ans = 0;
        vector<int> dp(A.size(), 0);
        for(int i=2; i < A.size() ; ++i)
        {
            if(A[i] - A[i-1] == A[i-1] - A[i-2])
                dp[i] = dp[i-1] + 1;
            ans +=  dp[i];
        }
        return ans;
    }
};


int main()
{
    vector<int> nums = {1,2,3,4,5};
    Solution s;
    int vec = s.numberOfArithmeticSlices(nums);
    cout << "vec:" << vec << endl;
    return 0;
}