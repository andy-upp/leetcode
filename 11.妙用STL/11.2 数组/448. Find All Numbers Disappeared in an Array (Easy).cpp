#include <vector>
#include <iostream>
using namespace std;

/*
输入是一个一维整数数组，输出也是一个一维整数数组，表示输入数组内没出现过的数字。
Input: [4,3,2,7,8,2,3,1]
Output: [5,6]
 * */

class Solution
{
public:
     vector<int> findDisappearedNumbers(vector<int>& nums)
     {
         vector<int> ans;
         for(auto& num: nums)
         {
             int pos = abs(num) - 1;
             if(nums[pos] > 0)
             {
                nums[pos] = -nums[pos];
             }
         }

         for(int i = 0; i < nums.size(); ++i)
         {
             if(nums[i] > 0)
                 ans.push_back(i+1);
         }
         return ans;

     }

};

int main()
{
    vector<int>  vec = {4,3,2,7,8,2,3,1}; //C++11 新的赋值方式，对所有容器都使用
    vector<int>  vec_;
    Solution s;
    vec_ = s.findDisappearedNumbers(vec);
    for(auto v: vec_)
        cout << v << endl;
    
    return 0;
}