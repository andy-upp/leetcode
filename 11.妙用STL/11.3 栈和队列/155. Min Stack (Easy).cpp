#include<stack>
#include<iostream>
using namespace std;

class Minstack 
{
public:
    Minstack() {}
    void push(int x)
    {
        s.push(x);
        if(min_s.empty() || min_s.top() >= x)
        {
            min_s.push(x);
        }
    }

    void pop()
    {
        if(!min_s.empty() && min_s.top() == s.top())
        {
            min_s.pop();
        }
        s.pop();
    }

    int top()
    {
        return s.top();
    }

    int getMin()
    {
        return min_s.top();
    }

private:
    stack<int> s, min_s;

};

int main()
{
    Minstack* minStack = new Minstack();
    minStack->push(-2);
    minStack->push(0);
    minStack->push(-3);
    cout << minStack->getMin() << endl;
    minStack->pop();
    cout << minStack->top() << endl;
    cout << minStack->getMin() << endl;

    return 0;
}