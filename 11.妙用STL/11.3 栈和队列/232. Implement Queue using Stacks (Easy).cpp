//
// Created by 李光辉 on 2021/1/10.
//

/*
尝试使用栈（stack）来实现队列（queue）。
*/
#include <stack>
#include <iostream>
using namespace std;


class MyQueue
{
public:
    MyQueue(){}

    void push(int x)
    {
        in.push(x);
    }

    int pop()
    {
        //借助out栈调度
        if(out.empty())
        {
            while (!in.empty())
            {
                int x = in.top();
                in.pop();
                out.push(x);
            }
        }
        //元素出队列
        int x = out.top();
        out.pop();

        //将元素重新放回
        while(!out.empty())
        {
            in.push(out.top());
            out.pop();
        }
        return x;
    }

    int peek(){
        while(!in.empty())
        {
            out.push(in.top());
            in.pop();
        }
        int s = out.top();

        while(!out.empty())
        {
            in.push(out.top());
            out.pop();
        }
        return s;
    }

    bool empty()
    {
        return in.empty()&&out.empty();
    }

private:
    stack<int> in, out;
};

int main()
{
    MyQueue* queue = new MyQueue();
    queue->push(1);
    queue->push(2);
    cout << queue->peek() << endl; // returns 1
    cout << queue->pop() << endl; // returns 1
    cout << queue->empty() << endl; // returns false
    delete queue;
    return 0;
 }